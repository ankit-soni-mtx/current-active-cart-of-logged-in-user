## Asset Description :
- This asset contains apex class which returns the current active cart model of the logged in user.
- CartModel.cls and CartItemModel.cls apex classes are used as Wrapper Class for the handler class.
- This apex class can be used in both LWC and Aura components.
- This has been used in project 7s:nVent B2B Commerce Cloud Implementation.

## Asset Use :
- This Cart Model can be used to retreive the Current Active Cart details of logged in user.
- Simply invoke the getCurrentCart() method in the handler from LWC/Aura component which will return the CartModel.