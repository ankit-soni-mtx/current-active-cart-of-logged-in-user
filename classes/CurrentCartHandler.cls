/**
 * Created By ankitSoni on 27/08/2020
 */
public class CurrentCartHandler {
    /**
     * Description : Get Current Cart Model of logged in user
     * @return : CartModel
     */
    @AuraEnabled
    public static CartModel getCurrentCart() {

        String userId = UserInfo.getUserId(); // sfid of the logged in user

        CartModel model = new CartModel();

        Map < String, Object > inputData = new Map < String, Object > {
            ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
            ccrz.ccApiCart.ACTIVECART => true,
            ccrz.ccApiCart.CARTTYPE => 'Cart',
            ccrz.ccApiCart.BYOWNER => userId,
            ccrz.ccApiCart.BYUSER => userId,
            ccrz.ccApi.SIZING => new Map <String,Object> {
                ccrz.ccAPIProduct.ENTITYNAME => new Map <String,Object> {
                    ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL
                }
            }
        };

        try {

            //Call Cloudcraze method to fetch cart with inputData defined above.
            Map < String, Object > outputData = ccrz.ccApiCart.fetch(inputData);

            if (outputData.get(ccrz.ccApiCart.CART_OBJLIST) != null) {
                List < Map < String, Object >> outputCartList = (List < Map < String, Object >> ) outputData.get(ccrz.ccApiCart.CART_OBJLIST);
                //outputCartList contains all the fields of the current cart (CC Cart Object)

                //Get All Products present in the cart
                List < Map < String, Object >> productList = (List < Map < String, Object >> ) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST);
                //productList contains all the products (CC Product) of the above fetched cart

                if (outputCartList.size() > 0) {

                    //collect the items in the cart
                    for (Map < String, Object > cartEntry: outputCartList) {

                        model.cartId = (String) cartEntry.get('sfid');
                        model.cartEncId = (String) cartEntry.get('encryptedId'); //ccrz__EncryptedId__c
                        model.totalPrice = (Decimal) cartEntry.get('totalAmount'); //ccrz__TotalAmount__c
                        model.totalQuantity = (Decimal) cartEntry.get('totalQuantity'); //ccrz__TotalQuantity__c
                        model.currencyISOCode = (String) cartEntry.get('currencyISOCode'); //ccrz__CurrencyISOCode__c

                        // get cart items in the cart
                        List < Map < String, Object >> cartItems = (List < Map < String, Object >> ) cartEntry.get('ECartItemsS');
                        for (Map < String, Object > item: cartItems) {

                            CartItemModel cartItem = new CartItemModel();

                            if ((String) item.get('cartItemType') == 'Major') {
                                itemCount += (Decimal) item.get('quantity');

                                cartItem.id = (String) item.get('sfid');
                                cartItem.productId = (String) item.get('product'); //ccrz__Product__c
                                cartItem.quantity = (Decimal) item.get('quantity'); //ccrz_Quantity__c
                                cartItem.netPrice = (Decimal) item.get('price'); //Net Price of the item (ccrz__Price__c)
                                cartItem.subTotal = (Decimal) item.get('subAmount');
                                cartItem.isAvailable = (String) item.get('itemStatus') == 'Available' ? true : false;
                                cartItem.currencyISOCode = (String) cartEntry.get('currencyISOCode'); //Add currency code

                                //Get Product data
                                for (Map < String, Object > productEntry: productList) {

                                    if ((String) item.get('product') == (String) productEntry.get('sfid')) {

                                        cartItem.productName = (String) productEntry.get('sfdcName');
                                        cartItem.productSkuCode = (String) productEntry.get('SKU');
                                        cartItem.productLeadTime = (Decimal) productEntry.get('leadTime');

                                        List < Map < String, Object >> mediaList = (List < Map < String, Object >> ) productEntry.get('EProductMediasS');
                                        cartItem.imageUrl = ProductImageUtility.getImageSrc(mediaList, 'Product Search Image');
                                        //ProductImageUtility Apex Class is present in other Asset
                                    }
                                }

                                model.items.add(cartItem);
                            }

                        }

                    }
                }
            }

        } catch (Exception ex) {
            System.debug('fetch cart Exception: ' + ex.getMessage());
        }

        System.debug('Cart Model ' + model);
        return model;
    }
}
