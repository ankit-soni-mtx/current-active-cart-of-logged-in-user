/**
 * Created By ankitSoni on 27/08/2020
 */
@IsTest
public class CurrentCartHandlerTest {

    @IsTest
    static void getCurrentCartTest(){

        User testUser = TestUtility.createCommunityUser();

        Test.startTest();        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            ccrz__E_ProductMedia__c productMedia = TestUtility.createProductMedia(true, product1.Id);
            
            ccrz__E_Cart__c cart = TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            insert cart;
            
            List<ccrz__E_CartItem__c> cartItems = TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            cartItems[0].ccrz__cartItemType__c = 'Major';
            insert cartItems;

            CartModel model = CurrentCartHandler.getCurrentCart();
            System.assertEquals(cart.Id, model.cartId);
            System.assertNotEquals(null, model.cartId);
        }
        Test.stopTest();
    }
    
}