/**
 * Created By ankitSoni on 27/08/2020
 */
public class CartItemModel {
    
    //Cart Item fields
    @AuraEnabled public String id;
    @AuraEnabled public Decimal netPrice;
    @AuraEnabled public Decimal quantity;
    @AuraEnabled public Decimal subTotal;
    @AuraEnabled public String currencyISOCode;
    @AuraEnabled public Boolean isAvailable;

    // Product fields
    @AuraEnabled public String productId;
    @AuraEnabled public String productName;
    @AuraEnabled public String productSkuCode;    
    @AuraEnabled public Decimal productLeadTime;
    @AuraEnabled public String imageUrl;


}